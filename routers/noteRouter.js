const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {authMiddleware} = require('../middlewares/authMiddleware');
const {validateNote} = require('../middlewares/validation');
const {createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  checkNote} = require('../controllers/notesController');

router.post('', authMiddleware,
    asyncWrapper(validateNote), asyncWrapper(createNote));
router.get('', authMiddleware, asyncWrapper(getNotes));
router.get('/:id', authMiddleware, asyncWrapper(getNote));
router.put('/:id', authMiddleware,
    asyncWrapper(validateNote), asyncWrapper(updateNote));
router.delete('/:id', authMiddleware, asyncWrapper(deleteNote));
router.patch('/:id', authMiddleware, asyncWrapper(checkNote));

module.exports = router;
