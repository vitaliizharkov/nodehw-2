const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const dotenv = require('dotenv');
dotenv.config();

const {User} = require('../models/userModel');

module.exports.registration = async (req, res) => {
  const {username, password} = req.body;

  const userExists = await User.findOne({username: username});

  if (userExists) {
    return res.status(400).json({message: 'This user already exists!'});
  }

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();

  res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await User.findOne({username});

  if (!user) {
    return res.status(400).json({message: `No user with 
    username '${username}' found!`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt.sign({username: user.username,
    _id: user._id}, process.env.JWT_SECRET);
  res.json({message: 'Success', jwt_token: token});
};
