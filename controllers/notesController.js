const {Note} = require('../models/noteModel');

const createNote = async (req, res) => {
  const {text} = req.body;

  if (!text) {
    return res.status(400).json({message: 'Bad request'});
  }

  const note = new Note({
    text: text,
    userId: req.user._id,
  });

  await note.save();

  res.status(200).json({message: 'Success'});
};

const getNotes = async (req, res) => {
  const {offset, limit} = req.query;
  const notes = await Note.find({userId: req.user._id})
      .skip(Number(offset))
      .limit(Number(limit));
  if (!notes) {
    return res.status(400).json({message: 'Notes were not found!'});
  }
  res.status(200).json({notes});
};

const getNote = async (req, res) => {
  const note = await Note.findById({_id: req.params.id});
  if (!note) {
    return res.status(400).json({message: 'Bad request'});
  }
  res.status(200).json({note: {
    _id: note._id,
    userId: note.userId,
    completed: note.completed,
    text: note.text,
    createdDate: note.createdDate,
  }});
};

const updateNote = async (req, res) => {
  const {text} = req.body;
  if (!text) {
    return res.status(400).json({message: 'Text is not valid'});
  }
  if ( !(await Note.findById(req.params.id))) {
    return res.status(400).json({message: 'Bad request'});
  }
  await Note.findByIdAndUpdate(req.params.id, {$set: {text: text}});
  res.status(200).json({message: 'Note updated successfully'});
};

const deleteNote = async (req, res) => {
  if (!(await Note.findByIdAndDelete(req.params.id))) {
    return res.status(400).json({message: 'Note was not found'});
  }
  res.status(200).json({message: 'Note deleted successfully'});
};

const checkNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  if (!note) {
    return res.status(400).json({message: 'Note was not found'});
  }
  note.completed = !note.completed;
  await note.save();

  res.status(200).json({message: 'Success'});
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  deleteNote,
  checkNote,
};
