const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserInfo = async (req, res) => {
  const profile = await User.findById(req.user._id);
  if (!profile) {
    return res.status(400).json({message: 'User was not found'});
  }

  res.status(200).json({user: {
    _id: req.user._id,
    username: profile.username,
    createdDate: profile.createdDate,
  }});
};

const deleteUser = async (req, res) => {
  const username = req.user.username;
  if ( !(await User.findOneAndDelete({username})) ) {
    return res.status(400).json({message: 'User was not found'});
  };
  res.status(200).json({message: 'Success'});
};

const changeUserPassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const profile = await User.findById(req.user._id);

  if (!profile) {
    return res.status(400).json({message: 'User was not found'});
  }
  if ( !(await bcrypt.compare(oldPassword, profile.password)) ) {
    return res.status(400).json({message: 'Old password is wrong'});
  }

  const newPass = await bcrypt.hash(newPassword, 10);
  await User.findOneAndUpdate({'password': profile.password},
      {password: newPass});
  res.status(200).json({message: 'Success'});
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
