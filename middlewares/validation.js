const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        .alphanum()
        .min(2)
        .max(30)
        .required(),

    password: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$'))
        .required(),
  });

  const validation = await schema.validateAsync(req.body);
  const {error} = validation;
  if (error) {
    return res.status(400).json({message: error.message});
  }
  next();
};

module.exports.validateNote = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .min(1)
        .required(),
  });

  await schema.validateAsync(req.body);
  next();
};
